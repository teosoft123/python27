class Base(object):
    pass


class Message(Base):

    class Header(object):

        def __init__(self, host):
            self.host = host

    def __init__(self, version):
        self.version = version
        self.h = Message.Header(self)
