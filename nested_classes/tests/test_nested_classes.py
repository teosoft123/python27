import unittest

from nested_classes.message import Message


class TestNestedClasses(unittest.TestCase):

    def test_creation_and_access(self):
        m = Message((0, 1))
        self.assertIsNotNone(m)
        self.assertEqual(m.h.host, m)
        self.assertEqual((0, 1), m.h.host.version)
