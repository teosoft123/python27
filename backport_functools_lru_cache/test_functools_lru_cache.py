import unittest

try:
    from functools import lru_cache
except ImportError:
    from backports.functools_lru_cache import lru_cache

database = {
    'ringo-starr': 39,
    'paul-mccartney': 38,
    'john-lennon': 41,
}

seen = set()


@lru_cache(maxsize=2)
def use_cache(first_name, last_name):
    key = first_name + '-' + last_name
    if key in seen:
        raise ValueError
    else:
        seen.add(key)
        return database[key]


class MyTestCase(unittest.TestCase):

    def test_use_cache(self):
        self.assertEqual(41, use_cache('john', 'lennon'))
        self.assertEqual(38, use_cache('paul', 'mccartney'))
        self.assertEqual(39, use_cache('ringo', 'starr'))
        # cache limit is set to 2, now john lennon is
        # evicted from the cache but present in seen set
        # and we expect the exception
        with self.assertRaises(ValueError):
            use_cache('john', 'lennon')
