import unittest

from private_classes.private_class_in_factory import factory


class TestPrivateClassInFactory(unittest.TestCase):
    def test_instantiation(self):
        foo = factory('')
        self.assertIsNotNone(foo)

    def test_if_it_works(self):
        foo = factory('John')
        self.assertEqual('Hello, John!', foo.greeting())
