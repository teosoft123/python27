def factory(name):

    class Foo:
        def __init__(self, n):
            self.n = n

        def greeting(self):
            return 'Hello, %s!' % (self.n,)

    # return new instance but delete parameters
    # to prevent them from being captured in outer scope
    f = Foo(name)
    del name
    return f
